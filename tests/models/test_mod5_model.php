<?php

class test_mod5_model extends CodeIgniterUnitTestCase
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('mod5/mod5_model');
	}

	public function setUp()
	{

    }

    public function tearDown()
	{

    }

	public function test_included()
	{
		$this->assertTrue(class_exists('mod5_model'));
	}

}
