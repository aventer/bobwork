<?php

class test_mod4_model extends CodeIgniterUnitTestCase
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('mod4/mod4_model');
	}

	public function setUp()
	{

    }

    public function tearDown()
	{

    }

	public function test_included()
	{
		$this->assertTrue(class_exists('mod4_model'));
	}

}
