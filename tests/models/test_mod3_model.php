<?php

class test_mod3_model extends CodeIgniterUnitTestCase
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('mod3/mod3_model');
	}

	public function setUp()
	{

    }

    public function tearDown()
	{

    }

	public function test_included()
	{
		$this->assertTrue(class_exists('mod3_model'));
	}

}
