<?php

class test_mod6_model extends CodeIgniterUnitTestCase
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('mod6/mod6_model');
	}

	public function setUp()
	{

    }

    public function tearDown()
	{

    }

	public function test_included()
	{
		$this->assertTrue(class_exists('mod6_model'));
	}

}
