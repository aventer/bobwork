<?php

class test_mod2_model extends CodeIgniterUnitTestCase
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('mod2/mod2_model');
	}

	public function setUp()
	{

    }

    public function tearDown()
	{

    }

	public function test_included()
	{
		$this->assertTrue(class_exists('mod2_model'));
	}

}
