### Code Generation in support of Modular Extensions development - BOB

# Code Generation - BOB

BOB is a code generator for the ITE HMVC application architecture. BOB runs as a php command line application.

The purpose of BOB is to generate boiler plate code with high degree of accuracy, in order to save time and establish best practice throughout the codebase.

### BOB Features:


help or [h]

	php bob help

	BOB 0.5.0
	The Console Tool for Bobwork 0.5.0
	Copyright (c) 2013 Integrity Engineering
	Licensed under the MIT License

	USAGE:

	php bob help[|h]
  
    - Prints this information.
    
    
module or [m]
    
	php bob module name
	
	Examples:
	
      php bob module user
      php bob m user

    - Generates a new Module with given name and the following file structure:

      Writing modules/user/controllers/user.php
      Writing modules/user/models/user_model.php
      Writing modules/user/views/user_view.php
      Writing modules/user/assets/js/user.js
      Writing modules/user/assets/css/user.css
      
      Writing tests/controllers/test_user.php
      Writing tests/views/test_user_view.php
      Writing tests/models/test_user_model.php


php bob global[|g]

  - Generates a new Module with given name and the following file structure:

    Writing modules/global/header.php
    Writing modules/global/footer.php
    Writing modules/global/navigation.php

    Examples:
    php bob global
    php bob g





### Notes:

BOB will not create the "modules" folder.

Instead it will alert you if the "modules" folder does not exist.

	ERROR: Cannot find modules directory. Are you in the project directory?


### FAQ

1. Q: Where should "bob" be installed so I can use it? A: The "bob" file should be in the same folder as your "modules" folder. In the new HMVC platform structure of ITE, that is the CodeIgniter project folder.


	