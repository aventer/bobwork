<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod6 extends MX_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('mod6/mod6_model');
    }

    public function index(  )
    {
        $this->load->view('mod6/mod6_view');
    }

}