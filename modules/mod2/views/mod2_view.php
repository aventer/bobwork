<?php include "modules/global/header.php"; ?>
<?php include "modules/global/navigation.php"; ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>modules/mod2/assets/css/mod2.css">

<!-- Comments for: mod2 -->

<header class="jumbotron subhead" id="overview">
    <div class="container">
        <h1>mod2</h1>
        <p class="lead">This is a View called by Controller.</p>
</header>

<div class="" id="">

</div>

<script src="<?php echo base_url(); ?>modules/mod2/assets/js/mod2.js"></script>

<?php include "modules/global/footer.php"; ?>