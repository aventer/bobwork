<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod2 extends MX_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('mod2/mod2_model');
    }

    public function index(  )
    {
        $this->load->view('mod2/mod2_view');
    }

}