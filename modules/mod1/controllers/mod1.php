<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod1 extends MX_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('mod1/mod1_model');
    }

    public function index(  )
    {
        $this->load->view('mod1/mod1_view');
    }

}