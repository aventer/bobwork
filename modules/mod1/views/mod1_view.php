<?php include "modules/global/header.php"; ?>
<?php include "modules/global/navigation.php"; ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>modules/mod1/assets/css/mod1.css">

<!-- Comments for: mod1 -->

<header class="jumbotron subhead" id="overview">
    <div class="container">
        <h1>mod1</h1>
        <p class="lead">This is a View called by Controller.</p>
</header>

<div class="" id="">

</div>

<script src="<?php echo base_url(); ?>modules/mod1/assets/js/mod1.js"></script>

<?php include "modules/global/footer.php"; ?>