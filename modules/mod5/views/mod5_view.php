<?php include "modules/global/header.php"; ?>
<?php include "modules/global/navigation.php"; ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>modules/mod5/assets/css/mod5.css">

<!-- Comments for: mod5 -->

<header class="jumbotron subhead" id="overview">
    <div class="container">
        <h1>mod5</h1>
        <p class="lead">This is a View called by Controller.</p>
</header>

<div class="" id="">

</div>

<script src="<?php echo base_url(); ?>modules/mod5/assets/js/mod5.js"></script>

<?php include "modules/global/footer.php"; ?>