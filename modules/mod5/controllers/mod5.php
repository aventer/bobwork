<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod5 extends MX_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('mod5/mod5_model');
    }

    public function index(  )
    {
        $this->load->view('mod5/mod5_view');
    }

}