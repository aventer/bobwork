<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod3 extends MX_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('mod3/mod3_model');
    }

    public function index(  )
    {
        $this->load->view('mod3/mod3_view');
    }

}