<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('home/home_model');
    }

    public function index(  )
    {
        $params = array(
            'a'=>'donkie',
            'b'=>'fart',
            'c'=>'yelp'
        );
        $this->load->view('home/home_view', array('params'=>$params));
    }

}