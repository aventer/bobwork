<?php include "modules/global/header.php"; ?>
<?php include "modules/global/navigation.php"; ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>modules/home/assets/css/home.css">

<!-- Comments for: home -->

<header class="jumbotron subhead" id="overview">
    <div class="container">
        <h1>home</h1>
        <p class="lead">Welcome to Bobland.</p>
</header>

<div class="container" id="home-page">
    <br />
    <?php echo modules::run('table', $params); ?>

</div>

<script src="<?php echo base_url(); ?>modules/home/assets/js/home.js"></script>

<?php include "modules/global/footer.php"; ?>