<?php include "modules/global/header.php"; ?>
<?php include "modules/global/navigation.php"; ?>

    <link rel="stylesheet" href="<?php echo base_url(); ?>modules/docs/assets/css/docs.css">

    <!-- Comments for: docs -->

    <header class="jumbotron subhead" id="overview">
        <div class="container">
            <h1>docs</h1>
            <p class="lead">Welcome to Bobwork</p>
    </header>

    <div class="container" id="introduction">

    <div class="row">
    <div class="span3 bs-docs-sidebar">
        <ul class="nav nav-list bs-docs-sidenav affix-top">
            <li><a href="#about"><i class="icon-chevron-right"></i> About</a></li>
            <li><a href="#history"><i class="icon-chevron-right"></i> History</a></li>
            <li><a href="#wishful-thinking"><i class="icon-chevron-right"></i> Wishful thinking</a></li>
            <li><a href="#challenges"><i class="icon-chevron-right"></i> Challenges</a></li>
            <li><a href="#framework"><i class="icon-chevron-right"></i> Framework</a></li>
        </ul>
    </div>
    <div class="span9">




    <!-- About
================================================== -->
    <section id="about">
        <div class="page-header">
            <h1>1. About Bobwork and Bob</h1>
        </div>
        <p class="lead">A framework and a generator. Code Generation in support of Modular Extensions development.</p>

        <h3>Code Generation - BOB</h3>

        BOB is a code generator for the ITE HMVC application architecture. BOB runs as a php command line application.

        The purpose of BOB is to generate boiler plate code with high degree of accuracy, in order to save time and establish best practice throughout the codebase.

        <h3>BOB Features</h3>

        <pre>


        help or [h]

        php bob help

        BOB 0.5.0
        The Console Tool for Bobwork 0.5.0
        Copyright (c) 2013 Integrity Engineering
        Licensed under the MIT License

        USAGE:

        php bob help[|h]

        - Prints this information.


        module or [m]

        php bob module name

        Examples:

        php bob module user
        php bob m user

        - Generates a new Module with given name and the following file structure:

        Writing modules/user/controllers/user.php
        Writing modules/user/models/user_model.php
        Writing modules/user/views/user_view.php
        Writing modules/user/assets/js/user.js
        Writing modules/user/assets/css/user.css

        Writing tests/controllers/test_user.php
        Writing tests/views/test_user_view.php
        Writing tests/models/test_user_model.php


        php bob global[|g]

        - Generates a new Module with given name and the following file structure:

        Writing modules/global/header.php
        Writing modules/global/footer.php
        Writing modules/global/navigation.php

        Examples:
        php bob global
        php bob g


        Notes:

        BOB will not create the "modules" folder.

        Instead it will alert you if the "modules" folder does not exist.

        ERROR: Cannot find modules directory. Are you in the project directory?


        FAQ

        1. Q: Where should "bob" be installed so I can use it?
            A: The "bob" file should be in the same folder as your "modules" folder.
            In the new HMVC platform structure of ITE, that is the CodeIgniter project folder.

        </pre>

    </section>


    <!-- About
    ================================================== -->
    <section id="history">
        <div class="page-header">
            <h1>2. History</h1>
        </div>
        <p class="lead">We started this project by accident.</p>
        <p>On a cold Monday morning in June 2013, 5 members of the Integrity Engineering team met to plan a sprint for a special project.</p>
        <p>We all want better tooling. Our goal was to build a Deployment Tool to help with Continuous Deployment and Development.</p>
        <p>We started with CiBonfire and soon realised it was too much of a moving target to rely on. That took the whole of Day1. On Day2 we were 20% into a sprint with no tools and no platform that offered a modular approach.</p>
        <p>But, that is probably out of context for you... so how did we get there?</p>
        <p>We just completed a research cycle on best practices which revealed a number of change priorities for the team.</p>
        <br />
        <h3>Research themes:</h3>
        <ul>
            <li>Intellectual Property Strategy and Management</li>
            <li>DevOps Collaboration</li>
            <li>Development Best Practice</li>
        </ul>
        <br />
        <h4>Intellectual Property Strategy:</h4>
        <ul>
            <li>Mature Codebase</li>
            <li>Modular Code Base</li>
            <li>Contribution to Open Source Projects</li>
            <li>Peer review</li>
            <li>Pair Programming</li>
            <li>Documentation</li>
        </ul>
        <br />
        <h4>DevOps Collaboration:</h4>
        <ul>
            <li>Automated Build</li>
            <li>Automated Testing</li>
            <li>Automated Deployment</li>
            <li>Git / Composer / Artisan</li>
            <li>One Click Deploy</li>
        </ul>
        <br />
        <h4>Development Best Practice:</h4>
        <ul>
            <li>The PHP stack, LHNMPRR</li>
            <li>Practice - phptherightway.com</li>
            <li>Architecture – 12Factor.net compliance</li>
            <li>Modularisation – Composer, Satis, Modules, Sparks</li>
            <li>Testing Unit/Behat – Travis-ci, phpunit, Codeception</li>
            <li>Standards – php-fig.org, HTML5, CSS3 ,JS5Strict</li>
            <li>Conventions – Bootstrap, CanJS, JQuery</li>
            <li>Framework – li3, Slim, CodeIgniter, Laravel, Symfony, Silex</li>
            <li>Platforms – CIBonfire, Drupal, Cartalyst, Sonata, CMF</li>
        </ul>
        <br />
        <h3>The conclusions</h3>
        <p>The team noticed the changes afoot in the PHP5 world and recognized many benefits spinning off from the revival spurred by <a href="http://getcomposer.org/">Composer</a> and <a href="https://packagist.org/">Packagist</a>.
            all of this was quite exiting, and they were eager to play along. However they had to take stock of what change would cost the business.</p>
        <p>The DevOps initiative was ahead of Development Best Practice and the Development team were not settled on a new platform and framework when the request to sprint on a "One-Click-Deploy" tool came in. In fact, the morning of the sprint, the team were still sharing findings and looking at possibilities.</p>
        <p>Two priorities the Development team were clear about improving were: <em>(1) Modularity</em> and (2) <em>Code Re-use</em>.</p>
        <h3>Enter Bonfire</h3>
        <p>The team looked at CIBonfire for inspiration. They were clear that they did not want CIBonFire in 0.6 or 0.7 version, but liked its HMVC Modular Design and Scaffolding Tools. It fitted their current toolset as they were already using CodeIgniter 2.x and were quite happy with its all round quality and community experience.
            A two day stab at using CIBonfire for the "One-Click-Deploy" tool proved it was really good at Rapid Application Development.</p>
        <p>Besides for its immaturity, there were other concerns. The direct coupling between Modules, Models and dataBase tables was far from ideal and the Database Classes where not quite their style. Some team members were partial to <a href="http://redbeanphp.com/">RedBean</a> or <a href="http://www.doctrine-project.org/">Doctrine</a> for the DataBase Access Layer (DBAL).</p>
        <p>The idea, on the morning in question, was to use this in-house project to evaluate some framework which offered good Modularity and Code Re-use potential from the ground up.</p>
        <p>Three frameworks were locked and loaded in the Dev VM:</p>
        <ul>
            <li>CodeIgniter with Bonfire Platform</li>
            <li>Symfony with no Platform</li>
            <li>Laravel with no Platform</li>
        </ul>
        <h3>The vote</h3>
        <p>On the day the team were informed that the sprint would have to yield a product as first priority. The evaluation of Symfony, Laravel and Bonfire was not the primary objective.</p>
        <p>Faced with that reality the team chose CIBonfire as a platform-close-to-home, and hoped to learn something about Modularity and Code Re-use best practice on the way.</p>
        <p>After the first day, the team were frustrated that neither CIBonfire 0.7 nor 0.6 worked sufficiently to promise a minimum viable product to the DevOps Product Owner.</p>
        <p>In retrospect, the troubles experienced the first day, could well be attributed to the Quirky VM that the developers were using. The DevOps initiative required the VM use for the project development process.</p>
        <p>So CIBonfire was out...</p>

        <h3>CodeIgniter HMVC</h3>
        <p>The core of the CIBonfire approach is HMVC as it is implemented in <a href="https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc">WiredDesignz</a>. So what do you do on a Monday evening with a project looming ahead and no Platform that encourages Modularity?</p>
        <p>You go back to basics, was the answer. Fire up the latest CodeIgniter, add the <a href="https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc">WiredDesignz HMVC</a> extension and learn why CIBonfire chose it as its foundation. With CIHMVC up and running, adding Twitter Bootstrap was the best strategy and a well considered choice for the Style Framework. But more about that later...</p>
        <p>The team added a Code Generator called Bob which added a standards based approach to their new framework implementation now called Bobwork.</p>
        <p>By the end of the project the team had also added a Unit Testing suite to the Bobwork framework. But they had not written any unit tests for the deployment tool.</p>
    </section>

    <!-- Wishful Thinking
   ================================================== -->
    <section id="wishful-thinking">
        <div class="page-header">
            <h1>3. Wishful thinking</h1>
        </div>
        <p class="lead">We all dream of unicorns...</p>
        <p>Now Integrity Engineering has its very own Platform in the making:</p>
        <ul>
            <li>What does the team want it to do for them?</li>
            <li>What does the business need it to do for it?</li>
            <li>What will make it better than CIBonfire?</li>
        </ul>
        <p>We all want better tooling. What if you had the time to make your own and be your own Product Owner? What would your list of features be?</p>

        <h3>Lego</h3> <h4><a href="http://en.wikipedia.org/wiki/Code_reuse">Code Reuse</a></h4>
        <p>Ad hoc code reuse has been practiced from the earliest days of programming. Programmers have always reused sections of code, templates, functions, and procedures.
            Code reuse is the idea that a partial computer program written at one time can be, should be, or is being used in another program written at a later time.
            The reuse of programming code is a common technique which attempts to save time and energy by reducing redundant work.</p>
        <p>The software library is a good example of code reuse. Programmers may decide to create internal abstractions so that certain parts of their program can be reused, or may create custom libraries for their own use. Some characteristics that make software more easily reusable are modularity, loose coupling, high cohesion, information hiding and separation of concerns.</p>
        <p>    For newly written code to use a piece of existing code, some kind of interface, or means of communication, must be defined. These commonly include a "call" or use of a subroutine, object, class, or prototype. In organizations, such practices are formalized and standardized by domain engineering aka software product line engineering.
            The general practice of using a prior version of an extant program as a starting point for the next version, is also a form of code reuse.</p>
        <p>    Some so-called code "reuse" involves simply copying some or all of the code from an existing program into a new one. While organizations can realize time to market benefits for a new product with this approach, they can subsequently be saddled with many of the same code duplication problems caused by cut and paste programming.</p>
        <p>    Many researchers have worked to make reuse faster, easier, more systematic, and an integral part of the normal process of programming. These are some of the main goals behind the invention of object-oriented programming, which became one of the most common forms of formalized reuse. A somewhat later invention is generic programming.</p>
        Another, newer means is to use software "generators", programs which can create new programs of a certain type, based on a set of parameters that users choose. Fields of study about such systems are generative programming and metaprogramming.</p>

        <h3>Machine City</h3> <h4><a href="http://en.wikipedia.org/wiki/Automation">Automation</a></h4>
        <p>Automating what repeats is the core strategy in the Modern era. The idea that repeating gestures are delegated to machines which perform the work accurately and tirelessly for very little cost is the driving principle for success in the market economy.</p>
        <p>Automation is the use of machines, control systems and information technologies to optimize productivity in the production of goods and delivery of services. The correct incentive for applying automation is to increase productivity, and/or quality beyond that possible with current human labor levels so as to realize economies of scale, and/or realize predictable quality levels.</p>
        <p>In the scope of industrialisation, automation is a step beyond mechanization. Whereas mechanization provides human operators with machinery to assist them with the muscular requirements of work, automation greatly decreases the need for human sensory and mental requirements while increasing load capacity, speed, and repeatability.</p>
        <p>Automation plays an increasingly important role in the world economy and in daily experience.</p>

        <h3>Raising the bar</h3> <h4><a href="http://en.wikipedia.org/wiki/Test_driven_development">Test-driven development</a></h4>

        <p>Test-driven development (TDD) is a software development process that relies on the repetition of a very short development cycle: first the developer writes an (initially failing) automated test case that defines a desired improvement or new function, then produces the minimum amount of code to pass that test, and finally refactors the new code to acceptable standards. </p>
        <p>Test-driven development is related to the test-first programming concepts of extreme programming, but more recently has created more general interest in its own right. Programmers also apply the concept to improving and debugging legacy code developed with older techniques.</p>
        <p>Complex systems require an architecture that meets a range of requirements. A key subset of these requirements includes support for the complete and effective testing of the system.</p>
        <h3>Modular Architecture</h3>

        <p>Effective modular design yields components that can be reused in multiple contexts.</p>
        <ul>
            <li>If only one version of a component exists in all products the company produces, it makes the codebase of the company easier to maintain.</li>
            <li>Version control and effective asset management as IP can join the one-click deploy strategy to keep all deployed systems up to date with the latest components.</li>
            <li>Reuse of components hardens the code for many use cases and makes each module more valuable.</li>
        </ul>

        <p>Effective modular design yields components that act like parts in the machine world.</p>
        <ul>
            <li>When innovation is employed in one module on one project, all other implementations of that module benefit.</li>
            <li>If a module fails, it fails in isolation.</li>
            <li>Mashups and rapid prototyping are made possible through modular architectures and effective module management as IP.</li>
        </ul>

        <p>Effective modular design yields components that share traits essential for effective TDD.</p>
        <ul>
            <li>High Cohesion ensures each unit provides a set of related capabilities and makes the tests of those capabilities easier to maintain.</li>
            <li>Low Coupling allows each unit to be effectively tested in isolation.</li>
            <li>Published Interfaces restrict Component access and serve as contact points for tests, facilitating test creation and ensuring the highest fidelity between test and production unit configuration.</li>
        </ul>
    </section>



    <!-- Challenges
================================================== -->
    <section id="challenges">
        <div class="page-header">
            <h1>4. Challenges</h1>
        </div>
        <p class="lead">The monster under the bed.</p>
        <p>We have been making applications for a while. What are our challenges. How can we overcome them? Most importantly how can we prepare a smoother path toward high quality product development.</p>

        <h3>Challenges we create</h3><h4><a href="http://en.wikipedia.org/wiki/Technical_debt">Technical debt</a></h4>
        <p>Technical debt (also known as design debt or code debt) is a neologistic metaphor referring to the eventual consequences of poor or evolving software architecture and software development within a codebase.
            The debt can be thought of as work that needs to be done before a particular job can be considered complete.</p>
        <p>As a change is started on a codebase, there is often the need to make other coordinated changes at the same time in other parts of the codebase or documentation. The other required, but uncompleted changes, are considered debt that must be paid at some point in the future.</p>
        <p>Common causes of technical debt include (a combination of):</p>
        <ul>
            <li>Business pressures, where the business considers getting something released sooner before all of the necessary changes are complete, builds up technical debt comprising those uncompleted changes.</li>
            <li>Lack of process or understanding, where businesses are blind to the concept of technical debt, and make decisions without considering the implications.</li>
            <li>Lack of building loosely coupled components, where functions are hard-coded; when business needs change, the software is inflexible.</li>
            <li>Lack of test suite, which encourages quick and less risky band-aids to fix bugs.</li>
            <li>Lack of documentation, where code is created without necessary supporting documentation. That work to create the supporting documentation represents a debt that must be paid.</li>
            <li>Lack of collaboration, where knowledge isn't shared around the organization and business efficiency suffers.</li>
            <li>Parallel Development at the same time on two or more branches can cause the build up of technical debt because of the work that will eventually be required to merge the changes into a single source base. The more changes that are done in isolation, the more debt that is piled up.</li>
            <li>Delayed Refactoring. As the requirements for a project evolve, it may become clear that parts of the code have gotten unwieldy and must be refactored in order to support future requirements. The longer that refactoring is delayed, and the more code is written to use the current form, the more debt that piles up that must be paid at the time the refactoring is finally done.</li>
        </ul>
        <p>"Interest payments" are both in the necessary local maintenance and the absence of maintenance by other users of the project. Ongoing development in the upstream project can increase the cost of "paying off the debt" in the future. One pays off the debt by simply completing the uncompleted work.</p>
        <p>The build up of technical debt is a major cause for projects to miss deadlines.</p>
        <p>It is difficult to estimate exactly how much work is necessary to pay off the debt. For each change that is initiated, an uncertain amount of uncompleted work is committed to the project. The deadline is missed when the project realizes that there is more uncompleted work (debt) than there is time to complete it in.</p>
        <p>To have predictable release schedules, a development team should limit the amount of work in progress in order to keep the amount of uncompleted work (or debt) small at all times.</p>

    </section>




    <!-- Framework
================================================== -->
    <section id="framework">
        <div class="page-header">
            <h1>5. Framework</h1>
        </div>
        <p class="lead">To be or not to be...</p>
        <p>We have all seen projects with enourmous technical debt. I believe we have all created projects with technical debt that still haunt us to this day.</p>
        <p>If it is so easy to accumulate "technical debt" and if the effects are so negative, how do we go about building a mature and debt-free codebase? If we want to do it on a budget, there is only one way... "stand on the shoulders of giants".</p>
        <p>Buy, Build or Augment, have always been the options any business faced when choosing software solutions for business problems.</p>

        <h3>Go shopping</h3><h4><a href="">Use what exists</a></h4>
        <p>During our research phase, we built simple applications with a number of frameworks and platforms in order to get a feel for their maturity.</p>
        <ul>
            <li>Framework – li3(Cake fame), Slim, CodeIgniter 2, Laravel 4, Symfony 2, Silex</li>
            <li>Platforms – CIBonfire, Drupal 8</li>
        </ul>

        <p>We came away with the clear impression that some frameworks had accumulated significant "technical debt". While others have paid their debt in a measured growth and expansion cycle which is well managed and repeats often.</p>
        <p> <a href="http://ellislab.com/codeigniter" >CodeIgniter 2</a> has a clean bill of health, but fell behind the language it is built on. It's a PHP4 framework wit no frills. But its going nowhere.</p>
        <p> <a href="http://lithify.me/" >Li3</a> had basically ground to a halt because its lofty goals could not be achieved in a short period of time and its supporters gave up waiting for it to be completed. Mounting "technical debt" is the main reason for its stagnation. It was built from scratch but is an excellent example of how a "not developed here" attitude can sink even the most promising projects.</p>
        <p> <a href="http://laravel.com/" >Laravel 4</a> is off to a good start because it is leveraging Symfony components and trading on the debt paid by Sensio Labs. It has great utilities and could take the place of CodeIgniter in the "entry-level" framework race. It's developers understand how to whoo novices (basically the majority of php scripting kids). Ruby on Rails have loured a lot of php intermediate programmers away from PHP in the past 5 years. Laravel 4 is trying to stem that tide. Laravel 4 facade strategy to clean up the IOC container syntax is interesting but counts against it in the long run because it locks your code into the framework syntax.</p>
        <p> <a href="https://drupal.org/home" > Drupal 8</a> is now leveraging Symfony components because its team realises the value of the Sensio Labs investment in Symfony. The Drupal Module codebase is not in good shape, on the other hand. Many Modules don't make it to stable versions across Drupal versions e.g. Drupal 6,7,8. Backward compatibility is weighing Druapl down. CMF may offer some CMS platform relief from the Symfony camp.</p>
        <p> <a href="http://symfony.com/what-is-symfony" >Symfony 2</a> and <a href="http://framework.zend.com/" >Zend Framework 2</a> are the most mature full stack frameworks available in PHP5 land.</p>
        <p> <a href="http://silex.sensiolabs.org/" >Silex</a> and <a href="http://www.slimframework.com/" >Slim 2</a> are without doubt the best <a href="http://microphp.org/" >micro frameworks</a> at the PHP5 revival rally. Comparing these two frameworks showed the value of leveraging of mature components. Silex has a clear advantage because of its reliance on Symfony components.</p>

        <p>Composer and Packageist have lit a flame of cooperation and renewal in the many PHP developers hearts. Package management is the key to gaining the true value of cooperation. The Ruby community are an excellent example of the benefits of paying the debt together.</p>

        <p>In fact <a href="https://packagist.org/" >Package Management</a> with <a href="http://getcomposer.org/" >Composer</a> of and the <a href="http://www.php-fig.org/" >[psr] standards of the Framework Interoperability Group</a> should be the foundation criteria leading to a framework partner choice. Yes I said "partner choice", because your development team and company will rely on your partner to make a debt free contribution to your product development efforts.</p>

        <p>If you are a noob, use <a href="http://laravel.com/" >Laravel 4</a>. If you are a geek, use <a href="http://silex.sensiolabs.org/" >Silex</a>. If you are a business use <a href="http://symfony.com/what-is-symfony" >Symfony 2</a> for in-house apps.</p>

        <h3>Roll your own</h3><h4><a href="http://fabien.potencier.org/article/50/create-your-own-framework-on-top-of-the-symfony2-components-part-1">Make your own framework</a></h4>


        <blockquote class="note"><p>
                This article is part of a series of articles that explains how to create a framework with
                the Symfony2 Components:
                <a href="http://fabien.potencier.org/article/50/create-your-own-framework-on-top-of-the-symfony2-components-part-1">1</a>,
                <a href="http://fabien.potencier.org/article/51/create-your-own-framework-on-top-of-the-symfony2-components-part-2">2</a>,
                <a href="http://fabien.potencier.org/article/52/create-your-own-framework-on-top-of-the-symfony2-components-part-3">3</a>,
                <a href="http://fabien.potencier.org/article/53/create-your-own-framework-on-top-of-the-symfony2-components-part-4">4</a>,
                <a href="http://fabien.potencier.org/article/54/create-your-own-framework-on-top-of-the-symfony2-components-part-5">5</a>,
                <a href="http://fabien.potencier.org/article/55/create-your-own-framework-on-top-of-the-symfony2-components-part-6">6</a>,
                <a href="http://fabien.potencier.org/article/56/create-your-own-framework-on-top-of-the-symfony2-components-part-7">7</a>,
                <a href="http://fabien.potencier.org/article/57/create-your-own-framework-on-top-of-the-symfony2-components-part-8">8</a>,
                <a href="http://fabien.potencier.org/article/58/create-your-own-framework-on-top-of-the-symfony2-components-part-9">9</a>,
                <a href="http://fabien.potencier.org/article/59/create-your-own-framework-on-top-of-the-symfony2-components-part-10">10</a>,
                <a href="http://fabien.potencier.org/article/60/create-your-own-framework-on-top-of-the-symfony2-components-part-11">11</a>,
                <a href="http://fabien.potencier.org/article/62/create-your-own-framework-on-top-of-the-symfony2-components-part-12">12</a>.</p>
        </blockquote>

        <p>Symfony2 is a reusable set of standalone, decoupled, and cohesive PHP
            components that solve common web development problems.</p>

        <p>Instead of using these low-level components, you can use the ready-to-be-used
            Symfony2 full-stack web framework, which is based on these components... or
            you can create your very own framework. This series is about the latter.</p>

        <blockquote class="note"><p>
                If you just want to use the Symfony2 full-stack framework, you'd better
                read its official <a href="http://symfony.com/doc">documentation</a> instead.</p>
        </blockquote>

        <h3>Why would you like to create your own framework?</h3>

        <p>Why would you like to create your own framework in the first place? If you
            look around, everybody will tell you that it's a bad thing to reinvent the
            wheel and that you'd better choose an existing framework and forget about
            creating your own altogether. Most of the time, they are right but I can think
            of a few good reasons to start creating your own framework:</p>

        <ul>
            <li><p>To learn more about the low level architecture of modern web frameworks in
                    general and about the Symfony2 full-stack framework internals in
                    particular;</p></li>
            <li><p>To create a framework tailored to your very specific needs (just be sure
                    first that your needs are really specific);</p></li>
            <li><p>To experiment creating a framework for fun (in a learn-and-throw-away
                    approach);</p></li>
            <li><p>To refactor an old/existing application that needs a good dose of recent
                    web development best practices;</p></li>
            <li><p>To prove the world that you can actually create a framework on your own
                    (... but with little effort).</p></li>
        </ul>

        <p>I will gently guide you through the creation of a web framework, one step at a
            time. At each step, you will have a fully-working framework that you can use
            as is or as a start for your very own. We will start with simple frameworks
            and more features will be added with time. Eventually, you will have a
            fully-featured full-stack web framework.</p>

        <p>And of course, each step will be the occasion to learn more about some of the
            Symfony2 Components.</p>

        <blockquote class="tip"><p>
                If you don't have time to read the whole series, or if you want to get started
                fast, you can also have a look at <a href="http://silex.sensiolabs.org/">Silex</a>, a micro-framework based on the
                Symfony2 Components. The code is rather slim and it leverages many aspects of
                the Symfony2 Components.</p>
        </blockquote>

        <p>Many modern web frameworks call themselves MVC frameworks. We won't talk about
            MVC here as the Symfony2 Components are able to create any type of frameworks,
            not just the ones that follow the MVC architecture. Anyway, if you have a look
            at the MVC semantics, this series is about how to create the Controller part
            of a framework. For the Model and the View, it really depends on your personal
            taste and I will let you use any existing third-party libraries (Doctrine,
            Propel, or plain-old PDO for the Model; PHP or Twig for the View).</p>

        <p>When creating a framework, following the MVC pattern is not the right goal.
            The main goal should be the Separation of Concerns; I actually think that this
            is the only design pattern that you should really care about. The fundamental
            principles of the Symfony2 Components are centered around the HTTP
            specification. As such, the frameworks that we are going to create should be
            more accurately labelled as HTTP frameworks or Request/Response frameworks.</p>



    </div>
    </div>

    </div>

    <script src="<?php echo base_url(); ?>modules/docs/assets/js/docs.js"></script>

<?php include "modules/global/footer.php"; ?>