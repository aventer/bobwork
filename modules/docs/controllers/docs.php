<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Docs extends MX_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('docs/docs_model');
    }

    public function index(  )
    {
        $this->load->view('docs/docs_view');
    }

}