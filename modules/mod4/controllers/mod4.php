<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod4 extends MX_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('mod4/mod4_model');
    }

    public function index(  )
    {
        $this->load->view('mod4/mod4_view');
    }

}