<?php include "modules/global/header.php"; ?>
<?php include "modules/global/navigation.php"; ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>modules/mod4/assets/css/mod4.css">

<!-- Comments for: mod4 -->

<header class="jumbotron subhead" id="overview">
    <div class="container">
        <h1>mod4</h1>
        <p class="lead">This is a View called by Controller.</p>
</header>

<div class="" id="">

</div>

<script src="<?php echo base_url(); ?>modules/mod4/assets/js/mod4.js"></script>

<?php include "modules/global/footer.php"; ?>