<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Bobwork</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/docs.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/js/google-code-prettify/prettify.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url(); ?>assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/favicon.png">

    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-146052-10']);
        _gaq.push(['_trackPageview']);
        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar">

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-transition.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-alert.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-modal.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-dropdown.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-scrollspy.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-tab.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-tooltip.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-popover.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-button.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-collapse.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-carousel.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-typeahead.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-affix.js"></script>

<script src="<?php echo base_url(); ?>assets/js/holder/holder.js"></script>
<script src="<?php echo base_url(); ?>assets/js/google-code-prettify/prettify.js"></script>

<script src="<?php echo base_url(); ?>assets/js/application.js"></script>
