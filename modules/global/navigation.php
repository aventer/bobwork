<!-- Navbar
================================================== -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="brand" href="<?php echo site_url(); ?>/home">Bobwork</a>
            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="<?php echo (uri_string() == "home") ? "active" : "" ; ?>" >
                        <a href="<?php echo site_url(); ?>/home">home</a>
                    </li>
                    <li class="<?php echo (uri_string() == "mod1") ? "active" : "" ; ?>" >
                        <a href="<?php echo site_url(); ?>/mod1">mod1</a>
                    </li>
                    <li class="<?php echo (uri_string() == "mod2") ? "active" : "" ; ?>" >
                        <a href="<?php echo site_url(); ?>/mod2">mod2</a>
                    </li>
                    <li class="<?php echo (uri_string() == "mod3") ? "active" : "" ; ?>" >
                        <a href="<?php echo site_url(); ?>/mod3">mod3</a>
                    </li>
                    <li class="<?php echo (uri_string() == "mod4") ? "active" : "" ; ?>" >
                        <a href="<?php echo site_url(); ?>/mod4">mod4</a>
                    </li>
                    <li class="<?php echo (uri_string() == "mod5") ? "active" : "" ; ?>" >
                        <a href="<?php echo site_url(); ?>/mod5">mod5</a>
                    </li>
                    <li class="<?php echo (uri_string() == "mod6") ? "active" : "" ; ?>" >
                        <a href="<?php echo site_url(); ?>/mod6">mod6</a>
                    </li>
                    <li class="<?php echo (uri_string() == "docs") ? "active" : "" ; ?>" >
                        <a href="<?php echo site_url(); ?>/docs">docs</a>
                    </li>
                    <li class="dropdown">
                        <a id="drop1" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">DropDown<b class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                            <li class="<?php echo (uri_string() == "home") ? "active" : "" ; ?>" role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url(); ?>/home">home</a></li>
                            <li role="presentation" class="divider"></li>
                            <li class="<?php echo (uri_string() == "mod1") ? "active" : "" ; ?>" role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url(); ?>/mod1">mod1</a></li>
                            <li class="<?php echo (uri_string() == "mod2") ? "active" : "" ; ?>" role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url(); ?>/mod2">mod2</a></li>
                            <li class="<?php echo (uri_string() == "mod3") ? "active" : "" ; ?>" role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url(); ?>/mod3">mod3</a></li>
                            <li class="<?php echo (uri_string() == "mod4") ? "active" : "" ; ?>" role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url(); ?>/mod4">mod4</a></li>
                            <li class="<?php echo (uri_string() == "mod5") ? "active" : "" ; ?>" role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url(); ?>/mod5">mod5</a></li>
                            <li class="<?php echo (uri_string() == "mod6") ? "active" : "" ; ?>" role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url(); ?>/mod6">mod6</a></li>
                            <li role="presentation" class="divider"></li>
                            <li class="<?php echo (uri_string() == "docs") ? "active" : "" ; ?>" role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url(); ?>/docs">docs</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>